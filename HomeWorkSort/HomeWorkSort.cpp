﻿// HomeWorkSort.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
using namespace std;


class Player      //Создаем класс, который создает переменные с именем игрока и его счетом 
{
private:
	char name[128];
	int score = 0;

public:

	int GetScore()    //Функция возвращает счет игрока
	{
		return score;
	}

	void SetPlayer()   //Функция при помощи который мы задаем параметры имя и счет
	{
		std::cout << "Player Name: ";
		std::cin >> name;
		std::cout << "Player Score: ";
		std::cin >> score;
	}

	void Show()   //Выводим полученные значения
	{
		std::cout << name << " - " << score << "\n";
	}
};

void bouble(Player* array, int numb)        // Функция сортировки
{
	while (numb--)
	{
		bool f = false;

		for (int y = 0; y < numb; y++)
		{
			if (array[y].GetScore() > array[y + 1].GetScore())
			{
				swap(array[y], array[y + 1]);
				f = true;
			}
		}
	}
}


	int main()
	{
		int numb;
		std::cout << "Number of player: ";
		std::cin >> numb;    //Задаем количесвто игроков

		Player* array = new Player[numb];   //Создаем динамический указатиель, использующий динамическую память

		for (int x = 0; x < numb; ++x)    //Вводим имя и счет для указанного ранее количества игроков
		{
			array[x].SetPlayer();
		}
		std::cout << "\n";

		for (int y = 0; y < numb; ++y)     //Возвращаем значения переменной счет для дальнейшей сортировки этих данных
		{

			array[y].GetScore();
			
		}
			
		bouble(array, numb);    //Вызов функции сортировки

		for (int y = 0; y < numb; ++y)  //Выводим отсортированные данные
		{
			array[y].Show();
		}

		delete [] array;   //Очищаем память
	}

